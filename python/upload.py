import os
import sys

from selenium import webdriver


def main(id):

    # Read password from file
    with open(id, 'r') as fp:
        password = fp.read()

    # Initialize driver
    options = webdriver.FirefoxOptions()
    options.headless = True
    driver = webdriver.Firefox(options=options)

    # Get website
    driver.get("http://95.179.150.119/")

    # Find element
    file_upload = driver.find_element_by_name("fileToUpload")
    password_field = driver.find_element_by_name("password")
    upload_button = driver.find_element_by_name("submit")

    # Send file
    file_upload.send_keys(os.getcwd() + '/strategies/id' + id + '.m')
    password_field.send_keys(password)
    upload_button.click()

    # Close driver
    driver.close()


if __name__ == "__main__":
    main(sys.argv[1])
