import numpy as np

from game import game


def main():
    # Initialize game
    kgame = game(agent_N=20, avg_karma=10, max_score=50,
                 U=np.array([1, 3]), p=np.array([3/4, 1/4]), alpha=0.8, tau=0.8)

    D = np.zeros((kgame.max_karma_,))
    D[10] = 1
    kgame.set_D(D)

    pi = np.zeros((2, kgame.max_karma_, kgame.max_karma_))
    pi[1, 1:, 1] = 1
    kgame.set_pi(pi)

    theta = np.ones((kgame.max_karma_,))
    kgame.set_theta(theta)

    kgame.step_1()
    print(kgame.pi_)

if __name__ == '__main__':
    main()
