import csv
import re

import requests
from bs4 import BeautifulSoup

URL = "http://95.179.150.119"
SCORE_FILE = "scoreboard.csv"
TIMELINE_FILE = "timeline.csv"


def write_scoreboard(scores):
    """ Write current scoreboard to csv file """

    fieldnames = ['id', 'rush', 'payoff', 'karma']

    with open(SCORE_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for score in scores:
            writer.writerow(score)


def parse_scoreboard(scoreboard):
    """
    Parse the scoreboard string

    Example data:
                ID              payoff      karma
    ----------------------------------------------
        id15920416 no rush         +0         10
        id15920416    rush         +0         10
    """

    raw_scores = scoreboard.split("\n")[3:-1]
    scores = []

    for raw_score in raw_scores:
        raw_data = raw_score.split()

        scores.append(
            {'id': raw_data[0], 'rush': 1 if raw_data[1] == 'rush' else 0, 'payoff': int(raw_data[-2]), 'karma': int(raw_data[-1])})

    if raw_scores:
        write_scoreboard(scores)
    else:
        print("No scoreboard data")

    return scores


def write_timeline(timeline):
    """ Write current timeline to csv file """

    fieldnames = ["time", "player_1", "karma_1", "rush_1",
                  "bid_1", "player_2", "karma_2", "rush_2", "bid_2", "winner"]

    with open(TIMELINE_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for event in timeline:
            writer.writerow(event)


def parse_history(history):
    """
    Parse the interaction history string

    Example data:
    12:21 > id15920416 (  0 karma, no rush) bidded   0, id19952142 (  9 karma,    rush) bidded   1, id19952142 won
    """

    raw_history = history.split("\n")[1:-1]
    timeline = []

    for event in raw_history:

        # Skip tax and airdrop line
        if "tax" in event:
            continue

        event_data = re.split('\(|\)|,|>', event)

        time = event_data[0].strip()

        player_1 = event_data[1].strip()
        karma_1 = int(event_data[2].strip().split()[0])
        rush_1 = not "no" in event_data[3]
        try:
            bid_1 = int(event_data[4].split()[1])
        except ValueError:
            bid_1 = -1

        player_2 = event_data[5].strip()
        karma_2 = int(event_data[6].strip().split()[0])
        rush_2 = not "no" in event_data[7]
        try:
            bid_2 = int(event_data[4].split()[1])
        except ValueError:
            bid_2 = -1

        winner = event_data[-1].split()[0]

        timeline.append(
            {"time": time, "player_1": player_1, "karma_1": karma_1, "rush_1": rush_1, "bid_1": bid_1, "player_2": player_2, "karma_2": karma_2, "rush_2": rush_2, "bid_2": bid_2, "winner": winner})

    write_timeline(timeline)

    return timeline


def main():

    # Get webpage
    page = requests.get(URL)

    # Parse html
    soup = BeautifulSoup(page.content, 'html.parser')

    # Find corresponding data
    pre = soup.find_all('pre')
    scoreboard = pre[0]
    history = pre[1]

    # Parse scoreboard
    scores = parse_scoreboard(str(scoreboard.string))

    # Parse history
    timeline = parse_history(str(history.string))


if __name__ == "__main__":
    main()
