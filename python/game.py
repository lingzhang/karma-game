import numpy as np
from numba import jit
# from .agent import agent


class game:
    def __init__(self, agent_N, avg_karma, max_score, U, p, alpha, tau):
        """
        Initialize class
        """

        # Parameters
        self.agent_N_ = agent_N
        self.avg_karma_ = avg_karma
        self.max_karma_ = agent_N * avg_karma + 1
        # self.max_score_ = max_score

        # Variables of the game
        self.K_ = np.arange(self.max_karma_)
        self.M_ = np.arange(self.max_karma_)
        self.O_ = np.arange(agent_N)

        # self.U_ = np.array([1, 3])
        # self.p_ = np.array([3/4, 1/4])
        assert(U.shape == p.shape)
        assert(sum(p) == 1)
        self.U_ = U
        self.p_ = p

        assert(alpha <= 1 and alpha >= 0)
        self.alpha_ = alpha

        assert(tau <= 1 and tau >= 0)
        self.tau_ = tau
        self.temp_ = 1

        # self.agents_ = []
        # for i in range(agent_N):
        #     self.agents_.append(agent(np.random.choice(
        #         self.U_, p=self.p_), avg_karma))

        super().__init__()

    def set_D(self, D):
        """
        Set initial values of the karma distribution
        """
        assert(D.shape == self.K_.shape and sum(D) == 1)
        self.D_ = D

    def set_pi(self, pi):
        """
        Set initial values of the message distribution
        """
        assert(pi.shape == (self.U_.shape[0],
               self.K_.shape[0], self.M_.shape[0]))
        self.pi_ = pi

    def set_theta(self, theta):
        """
        Set initial values of the cost of agent
        """
        assert(theta.shape == self.K_.shape)
        self.theta_ = theta

    @staticmethod
    def c(o, u, i):
        """
        Cost of interaction
        """
        return u * int(o == i)

    def gamma(self, k_i, m_i, k_j, m_j, i, j):
        """
        Interaction outcome as a probability distribution on O
        """

        m_i_tilda = np.min([m_i, k_i])
        m_j_tilda = np.min([m_j, k_j])

        P = np.zeros((self.agent_N_))
        if m_i_tilda > m_j_tilda:
            P[i] = 1
        elif m_i_tilda < m_j_tilda:
            P[j] = 1
        else:
            P[i] = 0.5
            P[j] = 0.5

        return P

    def Phi(self, k_i, m_i, k_j, m_j, o, i, k_prime):
        """
        Next value of k_prime
        """

        m_i_tilda = np.min([m_i, k_i])
        m_j_tilda = np.min([m_j, k_j])

        P = np.zeros((self.max_karma_,))
        if o == i:
            P[k_i - m_i_tilda - 1] = 1
        else:
            P[k_i + m_j_tilda - 1] = 1

        return P

    def rho(self, u_i, k_i, m_i, i):
        """
        Expected utility of choosing message m for an agent of type u, k
        Equation (6)
        """

        sum_4 = 0
        for k_j in self.K_:
            # For all probability of k_j
            D_k_j = self.D_[k_j]

            # Skip if 0
            if D_k_j == 0:
                continue

            sum_3 = 0
            for u_j_idx, u_j in enumerate(self.U_):
                # For all probability of u_j
                p_u_j = self.p_[u_j_idx]

                sum_2 = 0
                for m_j in self.M_:
                    # For all probability of m_j
                    pi_m_j = self.pi_[u_j_idx, k_j, m_j]

                    # Skip if 0
                    if pi_m_j == 0:
                        continue

                    sum_1 = 0

                    # Value of j does not matter
                    try:
                        gamma_o = self.gamma(k_i, m_i, k_j, m_j, i, i+1)
                    except:
                        gamma_o = self.gamma(k_i, m_i, k_j, m_j, i, i-1)

                    for o, p_o in enumerate(gamma_o):
                        # For all outcomes

                        # Skip if 0
                        if p_o == 0:
                            continue

                        c = self.c(o, u_i, i)

                        sum_0 = 0
                        for k_prime in self.K_:
                            # For every karma outcome k_prime
                            Phi_k_prime = self.Phi(
                                k_i, m_i, k_j, m_j, o, i, k_prime)
                            theta_k_prime = self.theta_[k_prime]

                            sum_0 += Phi_k_prime @ self.theta_

                        sum_1 += p_o * (c + self.alpha_ * sum_0)

                    sum_2 += pi_m_j * sum_1

                sum_3 += p_u_j * sum_2

            sum_4 += D_k_j * sum_3

        return sum_4

    def c_bar(self, k):
        """
        Expected cost of an interaction
        Equation (7)
        """

        sum_1 = 0
        for u_i_idx, u_i in enumerate(self.U_):
            # For all probability of u_i
            p_u_i = self.p_[u_i_idx]

            sum_0 = 0
            for m_i in self.M_:
                # For all probability of m_i
                pi_m_i = self.pi_[u_i, k, m_i]

                rho = self.rho(u_i, k, m_i, i=0)

                sum_0 += pi_m_i * rho

            sum_1 += p_u_i * sum_0

        return sum_1

    def pi(self, u_i, k_i):
        """
        Policy from expected utility
        Equation (9)
        """

        m_range = self.K_.copy()

        prob = np.array([np.e ** (-self.rho(u_i, k_i, m, 1)/self.temp_)
                for m in m_range])
        prob /= np.sum(prob)

        return prob.squeeze()

    @jit(forceobj=True)
    def step_1(self):
        """
        Compute the policy π from the previous policy, the stationary distribution D, and the expected utility θ. The policy is computed using (9) based on the values of ρ obtained from (6).
        """

        pi_new = np.zeros(self.pi_.shape)
        for u in self.U_:
            for k in self.K_:
                pi_new[np.where(self.U_ == u), k, :] = self.pi(u, k)

        pi_next = self.tau_ * pi_new + (1 - self.tau_) * self.pi_

        self.set_pi(pi_next)

        return


    def step_2(self):
        """
        Compute the transitions T from the policy π and the stationary distribution D.
        """