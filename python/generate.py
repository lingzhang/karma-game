import sys
from datetime import date, datetime, timedelta
from string import Template

import pandas as pd


def read_stat():
    # Summarize key statistics from csv files

    scoreboard = pd.read_csv('scoreboard.csv')
    timeline = pd.read_csv('timeline.csv')


def next_tues():
    # Find year, month, and date of next tuesday
    t = datetime.now()
    d = t.weekday()

    next_tuesday = t + timedelta((1 - d) % 7)
    next_tuesday = next_tuesday.replace(
        hour=14, minute=15, second=0, microsecond=0)

    if next_tuesday - t < timedelta(0):
        # For Tuesday after 14:15
        next_tuesday += timedelta(7)

    return next_tuesday.year, next_tuesday.month, next_tuesday.day


def main():

    # Variables to be replaced
    id = sys.argv[1]
    players = len(open('scoreboard.csv').readlines()) - 1
    year, month, date = next_tues()

    # Template mapping
    mapping = dict(id='id' + id, players=players,
                   year=year, month=month, date=date)

    fin = open('strategies/template.m', 'r')
    fout = open('strategies/id' + id + '.m', 'w')

    for line in fin:
        template = Template(line)
        fout.write(template.substitute(mapping))

    fin.close()
    fout.close()


if __name__ == "__main__":
    main()
