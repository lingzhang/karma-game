ID=

all: get-score test upload

get-score:
	pipenv run python python/scrape.py

generate:
	pipenv run python python/generate.py ${ID}

test: generate
	(cd ./strategies && octave --eval 'test(${ID})')

upload:
	pipenv run python python/upload.py ${ID}
