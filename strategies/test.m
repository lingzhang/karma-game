function test(id)
    close all;
    
    fh = str2func(strcat('id', num2str(id)));

    N = 20;
    bid_dat = zeros(2, N+1);

    for rush = [0, 1]
        for rep = 1:200
            for karma = 0:N
                bid = fh(rush, karma);
                assert(~mod(bid, 1), 'Bid is not an integer');
                assert(bid >= 0 && bid <= karma, ...
                    'Bid is out of bound: bid = %d, karma = %d', bid, karma);
                bid_dat(rush+1, karma+1) = bid_dat(rush+1, karma+1) + bid;
            end
        end
        figure;
        hold on;
        plot(0:N, bid_dat(rush+1, :)./200);
        fplot(@(x) x, [0, N], 'linewidth', 1);
        xlabel('Karma');
        ylabel('Bid');
        title(sprintf('Rush = %d', rush));
        saveas(gcf, sprintf('%d.png', rush));
    end
    
end
