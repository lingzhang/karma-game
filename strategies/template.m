function bid = ${id}(rush, karma)

    % Time to go
    ttg_end = time_to_go_end();
    ttg_hour = time_to_go_hour();

    % Expected number of rounds left
    players = ${players};
    rounds_end = expected_rounds(players, ttg_end);
    rounds_hour = expected_rounds(players, ttg_hour);

    % Urgency level
    if rush
        urgency = 3;
        e = max([...
		 max(0, karma - 10) / 5 + 1,...
		 karma / rounds_end,...
		 (karma - 8) / rounds_hour...
		]);
    else
        urgency = 1;
        e = max([...
		 max(0, karma - 10) / 5,...
		 karma / rounds_end,...
		 (karma - 8) / rounds_hour...
		]);
    end

    % Expected value of bidding

    bid = rand_bidding(e);

    % Set up bounds for bidding
    bid = max(0, min(karma, bid));

end

function bid = rand_bidding(e)
    % rand_bidding - Randomized bidding based on expected values

    low = floor(e);
    up = ceil(e);
    thres = e - low;

    p = rand();

    if p <= thres
        bid = up;
    else
        bid = low;
    end

end

function ttg = time_to_go_hour()
    % time_to_go - Find time left to end of the round

    ttg = mod(61 - gmtime(time()).min, 60) / 24 / 60;
end

function ttg = time_to_go_end()
    % time_to_go - Find time left to end of the round

    next_tues = datenum(${year}, ${month}, ${date}, 14, 15, 00);
    ttg = next_tues - now;
end

function rounds = expected_rounds(players, ttg)
    % expected_rounds - expected rounds to play based of time to go and number of players
    all_combo = nchoosek(players, 2);
    inc_combo = players - 1;

    prob = inc_combo / all_combo;
    rounds = prob * ceil(ttg * 24 * 60);
end
