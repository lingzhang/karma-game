# Karma-Game

This program participates in the Karma Game of ETH Zürich's Game Theory and Control course. It optimizes the participation by auto downloading statistics from the website, performing offline optimizations, and uploading to the server

## Setup

### Install Python Dependencies

- Install `pipenv`
- Run `pipenv install`
  
### Install Geckodriver

To use the auto upload functions, we need to run a headless Firefox browser.

- Install Firefox
- Install `geckodriver`

### Setup credentials

```sh
echo <your-password> > <your-uid>
```

## Usage

### Automatically perform everything
```sh
make all ID=<your-uid>
```

### Download Statistics

```sh
make get-score
```

### Generate Template

```sh
make generate ID=<your-uid>
```

### Test Your Script
```sh
make test ID=<your-uid>
```

### Upload Script

```sh
make upload ID=<your-uid>
```
